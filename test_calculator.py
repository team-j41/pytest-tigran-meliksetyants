import pytest
from calculator import calculator


@pytest.mark.parametrize("a, b, operator, expected", [
    (2, 3, "+", 5),
    (7, 5, "-", 2),
    (3, 4, "*", 12),
    (10, 5, "/", 2),
    (1, 11, "+", "Wrong Input number"),
    (0, 5, "-", "Wrong Input number"),
    (2, 3, "%", "Invalid operator"),
])
def test_calculator(a, b, operator, expected):
    assert calculator(a, b, operator) == expected